

let environment = require('./environment');
module.exports = {

    db:{
        name: "db",
        connector: "mongodb",
        url: environment.MONGOSTRING
    }
}