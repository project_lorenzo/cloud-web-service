
let json = {}
try{
    json = require('../config.json');
}catch(e){

}
module.exports = {
    BASE_URL: process.env.BASE_URL || '',
    PORT: process.env.PORT || json.PORT,
    MONGOSTRING: process.env.MONGOSTRING || json.MONGOSTRING
}