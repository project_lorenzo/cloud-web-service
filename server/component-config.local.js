
let environment = require('./environment');
module.exports =
{
    "loopback-component-explorer": {
      "mountPath": environment.BASE_URL+"/explorer",
      "generateOperationScopedModels": true
    }
}