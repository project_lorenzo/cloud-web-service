
let environment = require('./environment');

module.exports={
    "restApiRoot": environment.BASE_URL+"/api",
    "host": "0.0.0.0",
    "port": environment.PORT,
    "remoting": {
      "context": false,
      "rest": {
        "handleErrors": false,
        "normalizeHttpPath": false,
        "xml": false
      },
      "json": {
        "strict": false,
        "limit": "100kb"
      },
      "urlencoded": {
        "extended": true,
        "limit": "100kb"
      },
      "cors": false
    }
  }
  