'use strict';
const _ = require('lodash');

function tenant(Model) {
  

  function addTenant(ctx, next) {
    let userId = _.get(ctx,"options.accessToken.userId",null);
    if (userId) {
      ctx.instance.userId = userId;
      next();
    } else {
      return next(new Error('there is no current tenant'));
    }
  }

  function filterForDelete(ctx, next) {

    let userId = _.get(ctx,"options.accessToken.userId",null);
    if (userId) {
      var filter = {
        where: { userId: userId }
      };
      _.merge(ctx.query, filter);
      next();
    } else {
      return next(new Error('there is no current tenant'));
    }

    
  }

  function filterForQuery(ctx, next) {
    
    let userId = _.get(ctx,"options.accessToken.userId",null);
    if (userId) {
      var filter = {
        where: { userId: userId }
      };
      _.merge(ctx.query, filter);
      next();
    } else {
      return next(new Error('there is no current tenant'));
    }

  }


  // filter so that only models against this tenant are used
  Model.observe('access', filterForQuery);
  Model.observe('before delete', filterForDelete);

  // setup the model using the current tenant
  Model.observe('before save', addTenant);
}

module.exports = tenant;